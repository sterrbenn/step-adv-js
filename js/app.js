import { DOCTOR } from './constans.js';
import { visitContainerCards } from './elements.js';
"use strict";

class ModalAuth {
    constructor() {
        const placeForModal = document.querySelector('.placeformodal');
        this.getAllCards = null;
        this.modalContent = document.createElement('div');
        this.modalContent.classList.add('authorization-template');
        this.modalContent.innerHTML = `
            <img class="authimg" src="images/kadutsey.svg" alt="kadutsey">
            <form  class="authorization">
                Email: <input type="Login" placeholder="Введіть вашу пошту" class="emailinput">
                Password: <input type="Password" placeholder="Введіть ваш пароль" class="passwordinput">
                <input type="submit" value="Увійти" class="submit-auth-btn">
                <input type="button" value="✖" class="auth-cross">
            </form>
        `;

        placeForModal.appendChild(this.modalContent);

        const openModalBtn = document.querySelector('.enterbtn');
        this.openModalBtn = openModalBtn;

        const visitCreateBtn = document.querySelector('.visitcreatebtn');
        this.visitCreateBtn = visitCreateBtn;

        openModalBtn.addEventListener('click', this.open.bind(this));
        const closeModalBtn = this.modalContent.querySelector('.auth-cross');
        closeModalBtn.addEventListener('click', this.close.bind(this));

        const submitBtn = this.modalContent.querySelector('.submit-auth-btn');
        submitBtn.addEventListener('click', async (event) => {
            event.preventDefault();
            const getAllCards = new GetAllCards();
            await getAllCards.fetchCards(); 
            this.validation();
        });
    }

    open() {
        this.modalContent.style.display = 'block';
    }

    close() {
        this.modalContent.style.display = 'none';
    }

    validation() {
        const password = this.modalContent.querySelector('.passwordinput').value;
        const email = this.modalContent.querySelector('.emailinput').value;

        if (password === '' && email === '') {
            this.modalContent.style.display = 'none';
            const mainSpan = document.querySelector('.asktologin');
            mainSpan.innerHTML = 'No items have been added';
            this.openModalBtn.style.display = 'none';
            this.visitCreateBtn.style.display = 'block';
            const formFilter = document.querySelector('.form-filter');
            formFilter.style.display = 'block';
            visitContainerCards.style.display = 'flex';
            
        } else {
            alert('Невірний логін або пароль');
        }
    }
    handleEmptyCredentials() {
      
        this.modalContent.style.display = 'none';
        const mainSpan = document.querySelector('.asktologin');
        mainSpan.innerHTML = 'No items have been added';
        this.openModalBtn.style.display = 'none';
        this.visitCreateBtn.style.display = 'block';
        const formFilter = document.querySelector('.form-filter');
        formFilter.style.display = 'block';
        visitContainerCards.style.display = 'flex';
        if (this.getAllCards) {
            
            this.getAllCards.fetchCards();
        } else {
            console.error('getAllCards is not defined');
        }
    }
}


const modal = new ModalAuth();

class ModalCard {
    constructor() {
        const newVisitBtn = document.querySelector('.visitcreatebtn');
        this.newVisitBtn = newVisitBtn;

        newVisitBtn.addEventListener('click', this.addNewVisit.bind(this));

        const modalOverlay = document.querySelector('.visitCardoverlay');
        this.modalOverlay = modalOverlay;
        this.isModalOpen = false;
    }

    addNewVisit() {
        this.isModalOpen = true;
        const modalVisit = document.querySelector('.visitContainer');
        const template = document.querySelector('.cardTemplate');
        const modalCard = document.querySelector('.card-select');

        if (modalCard) {
            modalCard.style.display = "block";
            this.isModalOpen = true;
            this.modalOverlay.style.display = 'block';
        } else {
            const templateContent = template.content.cloneNode(true);
            modalVisit.appendChild(templateContent);
            const doctorSelect = document.querySelector('.doctorChoosing');
            doctorSelect.addEventListener('change', this.changeModal.bind(this));
            this.modalOverlay.style.display = "block";
            document.body.style.overflow = "hidden";
            this.closeModal();

            const token = 'd7e1a2f0-cae6-42e3-862c-16c130f002d0';
            const submitModal = new SubmitModal(token);
            submitModal.addListener();
        }
    }

    changeModal() {
        const doctorValue = document.querySelector('.doctorChoosing').value;

        const age = document.querySelector('.age');
        const diseases = document.querySelector('.diseases');
        const pressure = document.querySelector('.pressure');
        const fatIndex = document.querySelector('.fatIndex');
        const lastVisitDate = document.querySelector('.lastVisitDate');

        age.setAttribute('hidden', '');
        diseases.setAttribute('hidden', '');
        pressure.setAttribute('hidden', '');
        fatIndex.setAttribute('hidden', '');
        lastVisitDate.setAttribute('hidden', '');

        if (doctorValue === 'Therapist') {
            age.removeAttribute('hidden');
        }

        if (doctorValue === 'Cardiologist') {
            age.removeAttribute('hidden');
            diseases.removeAttribute('hidden');
            pressure.removeAttribute('hidden');
            fatIndex.removeAttribute('hidden');
        }

        if (doctorValue === 'Dentist') {
            lastVisitDate.removeAttribute('hidden');
        }
    }

    closeModal() {
        const closeModalBtn = document.querySelector('.closeModal');
        closeModalBtn.addEventListener('click', () => {
            const modalCard = document.querySelector('.card-select');
            modalCard.style.display = "none";
            this.isModalOpen = false;
            this.modalOverlay.style.display = 'none';
            const modalForm = document.querySelector(".doctorModal");
            modalForm.reset();
        });

        const submitModalBtn = document.querySelector('.submitBtn');
        submitModalBtn.addEventListener('click', () => {
            const modalCard = document.querySelector('.card-select');
            modalCard.style.display = "none";
            this.isModalOpen = false;
            this.modalOverlay.style.display = 'none';
        });

        const modalOverlay = document.querySelector('.visitCardoverlay');
        const modalCard = document.querySelector('.card-select');

        this.modalOverlay.addEventListener('click', function (event) {
            if (event.target === modalOverlay) {
                modalCard.style.display = "none";
                modalOverlay.style.display = 'none';
                const modalForm = document.querySelector(".doctorModal");
                modalForm.reset();
            }
        });
    }
}

const modalCard = new ModalCard();

class EditModalCard extends ModalCard {
    constructor(payload) {
        super(payload)
        this.payload = payload
        this.editForm = document.getElementById('edit-form');
        if (!this.editForm) {
            console.error("Element with id 'edit-form' not found.");
            return;
        }
        this.editForm.className = 'doctorModal card-select';
        this.editForm.innerHTML = '';
    }

    async renderCommonFilds() {

        this.visitTitle = document.createElement('input')
        this.visitTitle.value = this.payload.title

        this.visitTargetName = document.createElement('p')
        this.visitTargetName.textContent = 'Мета візиту:        '
        this.visitTarget = document.createElement('input')
        this.visitTarget.value = this.payload.target
        this.visitTargetName.append(this.visitTarget)


        this.shortDescriptionName = document.createElement('p')
        this.shortDescriptionName.textContent = 'Короткий опис візиту:'
        this.shortDescription = document.createElement('input')
        this.shortDescription.value = this.payload.shortDescription
        this.shortDescriptionName.append(this.shortDescription)


        this.urgencyName = document.createElement('p')
        this.urgencyName.textContent = 'Терміновість:'
        this.urgency = document.createElement('input')
        this.urgency.value = this.payload.urgency
        this.urgencyName.append(this.urgency)

        this.nameName = document.createElement('p')
        this.nameName.textContent = 'ПІБ:'
        this.name = document.createElement('input')
        this.name.value = this.payload.name
        this.nameName.append(this.name)

        this.buttonSubmit = document.createElement('button')
        this.buttonSubmit.textContent = 'Зберігти зміни'
        this.buttonSubmit.type = 'submit'


        this.editForm.addEventListener("submit", async (event) => {
            event.preventDefault();

            const id = this.payload.id;
            const title = this.visitTitle.value;
            const target = this.visitTarget.value;
            const shortDescription = this.shortDescription.value;
            const urgency = this.urgency.value;
            const age = this.age ? this.age.value : null;
            const name = this.name.value;
            const pressure = this.pressure ? this.pressure.value : null;
            const diseases = this.diseases ? this.diseases.value : null;
            const fatIndex = this.fatIndex ? this.fatIndex.value : null;
            const lastVisitDate = this.lastVisitDate ? this.lastVisitDate.value : null;

            try {
                const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
                    method: 'PUT',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer d7e1a2f0-cae6-42e3-862c-16c130f002d0`
                    },
                    body: JSON.stringify({
                        id: id,
                        title: title,
                        target: target,
                        shortDescription: shortDescription,
                        urgency: urgency,
                        age: age,
                        name: name,
                        pressure: pressure,
                        diseases: diseases,
                        fatIndex: fatIndex,
                        lastVisitDate: lastVisitDate,
                    })
                });

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                const visit = await response.json();
                this.closeModal();
                this.editForm.remove();
            } catch (error) {
                console.error('Error:', error);
            }
        });

        this.editForm.append(this.visitTitle)
        this.editForm.append(this.visitTargetName)
        this.editForm.append(this.shortDescriptionName)
        this.editForm.append(this.urgencyName)
        this.editForm.append(this.nameName)
        this.editForm.prepend(this.buttonSubmit)
    }

    renderTherapistModal() {
        if (!this.editForm) {
            console.error("Element with id 'edit-form' not found.");
            return;
        }
        this.renderCommonFilds()

        this.ageName = document.createElement('p')
        this.ageName.textContent = 'Вік:'
        this.age = document.createElement('input')
        this.age.value = this.payload.age
        this.ageName.append(this.age)

        this.editForm.append(this.ageName)

    }
    renderCardiologistModal() {
        if (!this.editForm) {
            console.error("Element with id 'edit-form' not found.");
            return;
        }
        this.renderCommonFilds()

        this.ageName = document.createElement('p')
        this.ageName.textContent = 'Вік:'
        this.age = document.createElement('input')
        this.age.value = this.payload.age
        this.ageName.append(this.age)

        this.pressureName = document.createElement('p')
        this.pressureName.textContent = 'Звичайний тиск:'
        this.pressure = document.createElement('input')
        this.pressure.value = this.payload.pressure
        this.pressureName.append(this.pressure)

        this.diseasesName = document.createElement('p')
        this.diseasesName.textContent = 'Індекс маси тіла:'
        this.diseases = document.createElement('input')
        this.diseases.value = this.payload.diseases
        this.diseasesName.append(this.diseases)


        this.fatIndexName = document.createElement('p')
        this.fatIndexName.textContent = 'Перенесені захворювання серцево-судинної системи:'
        this.fatIndex = document.createElement('input')
        this.fatIndex.value = this.payload.fatIndex
        this.fatIndexName.append(this.fatIndex)

        this.editForm.append(this.ageName)
        this.editForm.append(this.pressureName)
        this.editForm.append(this.diseasesName)
        this.editForm.append(this.fatIndexName)

    }
    renderDantistModal() {
        if (!this.editForm) {
            console.error("Element with id 'edit-form' not found.");
            return;
        }
        this.renderCommonFilds()

        this.lastVisitDateName = document.createElement('p')
        this.lastVisitDateName.textContent = 'Індекс маси тіла:'
        this.lastVisitDate = document.createElement('input')
        this.lastVisitDate.value = this.payload.lastVisitDate
        this.lastVisitDateName.append(this.lastVisitDate)

        this.editForm.append(this.lastVisitDateName)
    }


}


class SubmitModal {
    constructor(token) {
        this.token = token;
    }

    addListener() {
        const form = document.getElementById('submitBtnForm');
        form.addEventListener('submit', this.submitForm.bind(this));
    }

    submitForm(event) {
        event.preventDefault();

        const visitTitle = document.querySelector('.doctorChoosing').value;
        const visitTarget = document.querySelector('.visitTarget').value;
        const shortDescription = document.querySelector('.shortDescription').value;
        const urgency = document.querySelector('.urgency').value;
        const age = document.querySelector('.ageValue').value;
        const name = document.querySelector('.name').value;
        const pressure = document.querySelector('.pressure input').value;
        const diseases = document.querySelector('.diseases input').value;
        const fatIndex = document.querySelector('.fatIndex input').value;
        const lastVisitDate = document.querySelector('.lastVisitDate input').value;

        fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${this.token}`
            },
            body: JSON.stringify({
                title: visitTitle,
                target: visitTarget,
                shortDescription: shortDescription,
                urgency: urgency,
                age: age,
                name: name,
                pressure: pressure,
                diseases: diseases,
                fatIndex: fatIndex,
                lastVisitDate: lastVisitDate,
                status: 'відкритий' 
            })
        })
            .then(response => response.json())
            .then(visit => {
                const modalCardInstance = new ModalCard();
                modalCardInstance.closeModal();

                const modalForm = document.querySelector('.doctorModal');
                modalForm.reset();

               

                switch (visit.title) {
                    case DOCTOR.THERAPIST:
                        visit.status = 'відкритий';
                        new VisitTherapist(visit).renderVisitCard();
                        break;
                    case DOCTOR.CARDIOLOGIST:
                        visit.status = 'відкритий';
                        new VisitCardiologist(visit).renderVisitCard();
                        break;
                    case DOCTOR.DENTIST:
                        visit.status = 'відкритий';
                        new VisitDentist(visit).renderVisitCard();
                        break;
                }
            })
            .catch(error => {
                console.error('Помилка:', error);
            });
    }
}

class Visit {
    constructor(payload) {
        if (!payload || !payload.hasOwnProperty('status')) {
            payload = {
                ...payload,
                status: 'відкритий'
            };
        }
        this.payload = payload;

        this.visitCard = document.createElement('div');
        this.visitNameClient = document.createElement('p')
        this.visitNameDoctor = document.createElement('p')
        this.visitCardShowMore = document.createElement('div')
        this.visitDroptaun = document.createElement('p')
        this.visitPurpose = document.createElement('p')
        this.visitDescription = document.createElement('p')
        this.buttonShowMore = document.createElement('button')
        this.deleteCard = document.createElement('button')
    }

    renderVisitCard() {
        this.visitCard.className = 'visit-card';
        this.visitCard.id = this.payload.id;


        this.visitNameClient.className = 'name-client';
        this.visitNameClient.textContent = `ПІБ: ${this.payload.name}`;


        this.visitNameDoctor.className = 'name-doctor';
        this.visitNameDoctor.textContent = this.payload.title;

        const visitStatus = document.createElement('div');
        visitStatus.className = 'visit-status';
        visitStatus.innerHTML = `
            <label for="status-${this.payload.id}">Статус:</label>
            <select id="status-${this.payload.id}" class="status-select">
                <option value="open" ${this.payload.status === 'open' ? 'selected' : ''}>Відкритий</option>
                <option value="completed" ${this.payload.status === 'completed' ? 'selected' : ''}>Закритий</option>
            </select>
        `;

        visitStatus.querySelector('option[value="open"]').setAttribute('selected', 'selected');


        visitStatus.addEventListener('change', (event) => {
            this.updateStatus(event.target.value);
        });



        

        

        this.visitCardShowMore.className = 'visitCard-container-showMore';
        this.visitCardShowMore.hidden = true;

        this.visitDroptaun.className = 'visit-droptaun';
        this.visitDroptaun.textContent = `Терміновість візиту: ${this.payload.urgency}`;

        this.visitPurpose.className = 'visit-purpose';
        this.visitPurpose.textContent = `Мета візиту: ${this.payload.target}`;

        this.visitDescription.className = 'visit-description';
        this.visitDescription.textContent = `Короткий опис візиту: ${this.payload.shortDescription}`;

        this.buttonShowMore.className = 'visit-card-button';
        this.buttonShowMore.id = 'show-more-card';
        this.buttonShowMore.textContent = 'Показати більше';
        this.buttonShowMore.addEventListener('click', (event) => {
            this.visitCardShowMore.hidden = false
        })

        this.buttonEditCard = document.createElement('button');
        this.buttonEditCard.className = 'visit-card-button';
        this.buttonEditCard.id = 'edit-card';
        this.buttonEditCard.textContent = 'Редагувати';

        this.buttonEditCard.addEventListener('click', () => {

            switch (this.payload.title) {
                case DOCTOR.THERAPIST:
                    new EditModalCard(this.payload).renderTherapistModal();
                    break;
                case DOCTOR.CARDIOLOGIST:
                    new EditModalCard(this.payload).renderCardiologistModal();
                    break;
                case DOCTOR.DENTIST:
                    new EditModalCard(this.payload).renderDantistModal();
                    break;
            }

        });

        this.deleteCard.className = 'delete-card';
        this.deleteCard.id = 'delete-card';
        this.deleteCard.textContent = 'X';
        this.deleteCard.onclick = (event) => {

            fetch(`https://ajax.test-danit.com/api/v2/cards/${this.payload.id}`, {
                method: 'DELETE',
                headers: {
                    'Authorization': `Bearer d7e1a2f0-cae6-42e3-862c-16c130f002d0`
                },
            }).then(response => {
                if (response.ok == true) {
                    
                    this.visitCard.remove()

                }
            })

        }

        this.visitCard.append(this.visitNameClient, this.visitNameDoctor, visitStatus, this.visitCardShowMore, this.buttonShowMore, this.buttonEditCard, this.deleteCard);
        this.visitCardShowMore.append(this.visitDroptaun, this.visitPurpose, this.visitDescription);

        visitContainerCards.prepend(this.visitCard);
    }

    updateStatus(newStatus) {
        this.payload.status = newStatus;
        this.sendUpdateRequest();
        const visitStatusSelect = document.getElementById(`status-${this.payload.id}`);
        visitStatusSelect.value = newStatus;

    
       
    }
    async sendUpdateRequest() {
        try {
            const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${this.payload.id}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer d7e1a2f0-cae6-42e3-862c-16c130f002d0`
                },
                body: JSON.stringify({
                    id: this.payload.id,
                    status: this.payload.status
                })
            });
            console.log('Request sent to update status:', response);
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
    
            console.log('Status updated successfully');
            this.updateInterface();
        } catch (error) {
            console.error('Error updating status:', error);
        }
    }
    updateInterface() {
        

        const statusElement = document.querySelector(`#status-${this.payload.id}`);
        console.log('Status element:', statusElement);
    
        if (statusElement) {
            statusElement.value = this.payload.status; 
            console.log('Status updated successfully:', this.payload.status);
        console.log('Updated status element value:', statusElement.value);
        } else {
            console.error('Status element not found');
        }
    }
    


}


class VisitTherapist extends Visit {
    constructor(payload) {
        super()
        this.payload = payload

        this.informationAge = document.createElement('p')
        this.informationAge.textContent = `Вік: ${this.payload.age}`
        this.visitCardShowMore.prepend(this.informationAge)
    }
}

class VisitCardiologist extends Visit {
    constructor(payload) {
        super()
        this.payload = payload

        this.indexBodyMass = document.createElement('p');
        this.indexBodyMass.textContent = `Індекс маси тіла: ${this.payload.fatIndex}`;
        this.pressure = document.createElement('p');
        this.pressure.textContent = `Звичайний тиск: ${this.payload.pressure}`;
        this.diseases = document.createElement('p');
        this.diseases.textContent = `Перенесені захворювання серцево-судинної системи: ${this.payload.diseases}`;
        this.informationAge = document.createElement('p')
        this.informationAge.textContent = `Вік: ${this.payload.age}`
        this.visitCardShowMore.prepend(this.pressure);
        this.visitCardShowMore.prepend(this.indexBodyMass);
        this.visitCardShowMore.prepend(this.diseases);
        this.visitCardShowMore.prepend(this.informationAge);
    }
}

class VisitDentist extends Visit {
    constructor(payload) {
        super()
        this.payload = payload

        this.lastVisitDate = document.createElement('p');
        this.lastVisitDate.textContent = `Дата останнього відвідування: ${this.payload.lastVisitDate}`;
        this.visitCardShowMore.prepend(this.lastVisitDate);
    }
}
class GetAllCards {
    constructor() {
        this.apiUrl = 'https://ajax.test-danit.com/api/v2/cards';
        this.token = 'd7e1a2f0-cae6-42e3-862c-16c130f002d0';
        this.cards = [];
    }

    async fetchCards() {
        try {
            const response = await fetch(this.apiUrl, {
                headers: {
                    'Authorization': `Bearer ${this.token}`
                }
            });
            if (!response.ok) {
                throw new Error('Network response was not ok ' + response.statusText);
            }
            const data = await response.json();
            data.forEach(card => {
                card.status = card.status || 'відкритий';
            });
        
            this.cards = data;  
            this.renderCards(this.cards); 
        } catch (error) {
            console.error('Fetch error: ', error);
        }
    }


    renderCards() {
        visitContainerCards.innerHTML = '';
        this.cards.forEach(card => {
            switch (card.title) {
                case DOCTOR.THERAPIST:
                    new VisitTherapist(card).renderVisitCard();
                    break;
                case DOCTOR.CARDIOLOGIST:
                    new VisitCardiologist(card).renderVisitCard();
                    break;
                case DOCTOR.DENTIST:
                    new VisitDentist(card).renderVisitCard();
                    break;
            }
        });
    }
}


function translateStatus(status, toEnglish = true) {
    if (toEnglish) {
        switch(status) {
            case 'відкритий':
                return 'open';
            case 'completed':
                return 'completed';
            default:
                return status;
        }
    } else {
        switch(status) {
            case 'open':
                return 'відкритий';
            case 'completed':
                return 'completed';
            default:
                return status;
        }
    }
}


async function filterCards(filters) {
    try {
        
        const response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
            method: 'GET',
            headers: {
                'Authorization': `Bearer d7e1a2f0-cae6-42e3-862c-16c130f002d0`
            }
        });
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        const cards = await response.json();
        
        let filteredCards = cards;
        if (!filters.status) {
            filters.status = 'open';
        }

    
        if (filters.status && filters.status !== 'all') {
            const translatedStatus = translateStatus(filters.status, false); 
            
            if (filters.status === 'all') {
                filteredCards = filteredCards.filter(card => card.status === 'open' || card.status === 'completed');
            } else {
                filteredCards = filteredCards.filter(card => card.status === filters.status || card.status === translatedStatus);
            }
        }

        if (filters.priority !== 'all') {
            filteredCards = filteredCards.filter(card => card.urgency === filters.priority);
        }

        if (filters.search) {
            filteredCards = filteredCards.filter(card => {
                const titleMatch = card.target && typeof card.target === 'string' && card.target.toLowerCase().includes(filters.search.toLowerCase());
                const descriptionMatch = card.shortDescription && typeof card.shortDescription === 'string' && card.shortDescription.toLowerCase().includes(filters.search.toLowerCase());
                return titleMatch || descriptionMatch;
            });
    
        }

        
        visitContainerCards.innerHTML = '';

        filteredCards.forEach(card => {
           
            switch (card.title) {
                case DOCTOR.THERAPIST:
                   
                    new VisitTherapist(card).renderVisitCard();
                    break;
                case DOCTOR.CARDIOLOGIST:
                    
                    new VisitCardiologist(card).renderVisitCard();
                    break;
                case DOCTOR.DENTIST:
                  
                    new VisitDentist(card).renderVisitCard();
                    break;
                    
            }
        });

    } catch (error) {
        console.error('Error fetching or filtering cards:', error);
    }
}

const applyFiltersBtn = document.getElementById('applyFiltersBtn');
console.log(applyFiltersBtn);

applyFiltersBtn.addEventListener('click', async function() {
    try {
        const filters = {
            status: document.getElementById('status').value,
            priority: document.querySelector('input[name="priority"]:checked').value,
            search: document.getElementById('search').value
        };

        
        await filterCards(filters);

    } catch (error) {
        console.error('Error applying filters:', error);
        
    }


});












